# Docker Summary  
Docker is a tool that helps that **simplifies development, testing, and deployment** of softwares by building a lightweight and portable container. A containers packages up a software with all the parts it needs and ensures smooth running on any other machine.  

## Terminologies  


| SL. No | Term | Definition |
| ------ | ------ | ------ |
| 1.  | Docker Image  |  A file comprised of all the **dependancies** needed to execute a code in a Docker container. |
| 2.  | Container   | A container packages up the **code and all its dependencies**.   |
| 3.  | Docker Hub   | Docker Hub is a hosted repository service for finding and sharing container images. |
 

## Why Docker over VM?  
* VMs are more resource intensive.
* Reboots quickly.
* Better performance.  


## Basic Commands
| Command | Definition |
| ------ | ------ |
| docker ps | To view all the containers that are running on the Docker Host.  |
| docker start | To start any stopped container(s).  |
| docker stop | To stop any running container(s).  |
| docker run | To create containers from docker images.   |
| docker rm | To delete container(s). |

## Docker Workflow
 ![DockerWorkflow](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/docker-application-development-process/media/docker-app-development-workflow/life-cycle-containerized-apps-docker-cli.png)

