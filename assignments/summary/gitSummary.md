# Git Summary  
Git is a distributed **version-control system** for tracking changes in source code during software development. It is designed for **coordinating work** among programmers, and helps to keep track of revisions.  

![Git Branches](/uploads/9ec3eb33daf797bf6e345ee24df778e1/Your_Work.png)
## Vocabulary


| SL. No | Term | Definition |
| ------ | ------ | ------ |
|1. | Repository  | They contain a collection of files of **different versions** of a Project.  |
|2. | GitLab | A complete DevOps platform, delivered as a single application.It provides unlimimted free open and private repositories, issue-following capabilities, and wikis.|
|3. | Commit | **saving**. They only exist on your **local** machine until it is pushed to a remote repository.|
|4. | Push | A commit is synced only when it is pushed.|
|5. | Branch | Separate instances of the code that are different from the main codebase. |
|6. | Merge | When a branch is ready to become part of the primary codebase, it will be merged into the master branch.|
|7. | Clone | Cloning refers to making an **exact copy** of an online repository on your local machine.|
|8. | Forking | Forking refers to creating an entirely new repo of a code under your **own name**|



## Git Internals  

![git_states_of_files](/uploads/96fde5426911b20514bb25f1ce106aa0/git_states_of_files.png)

## Git Workflow  

![GIT Workflow](/uploads/3bedd71d801c30dc18fc7276ebf73083/ADD.png)
